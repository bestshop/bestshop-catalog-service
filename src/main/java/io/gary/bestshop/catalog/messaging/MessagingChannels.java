package io.gary.bestshop.catalog.messaging;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface MessagingChannels {

    String PRODUCT_CREATED_OUTPUT = "productCreatedOutput";

    @Output(PRODUCT_CREATED_OUTPUT)
    MessageChannel productCreatedOutput();


    String PRODUCT_UPDATED_OUTPUT = "productUpdatedOutput";

    @Output(PRODUCT_UPDATED_OUTPUT)
    MessageChannel productUpdatedOutput();


    String PRODUCT_REVIEW_ADDED_OUTPUT = "productReviewAddedOutput";

    @Output(PRODUCT_REVIEW_ADDED_OUTPUT)
    MessageChannel productReviewAddedOutput();


    String ORDER_COMPLETED_INPUT = "orderCompletedInput";

    @Input(ORDER_COMPLETED_INPUT)
    SubscribableChannel orderCompletedInput();
}
