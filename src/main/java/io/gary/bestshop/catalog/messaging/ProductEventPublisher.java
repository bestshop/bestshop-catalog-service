package io.gary.bestshop.catalog.messaging;

import io.gary.bestshop.messaging.dto.ProductDto;
import io.gary.bestshop.messaging.dto.ReviewDto;
import io.gary.bestshop.messaging.event.product.ProductCreatedEvent;
import io.gary.bestshop.messaging.event.product.ProductReviewAddedEvent;
import io.gary.bestshop.messaging.event.product.ProductUpdatedEvent;
import io.gary.bestshop.catalog.domain.Product;
import io.gary.bestshop.catalog.domain.Review;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Component;

import static org.springframework.messaging.support.MessageBuilder.withPayload;

@Component
@RequiredArgsConstructor
public class ProductEventPublisher {

    private final MessageChannel productCreatedOutput;

    private final MessageChannel productUpdatedOutput;

    private final MessageChannel productReviewAddedOutput;

    public Product publishProductCreatedEvent(Product product) {

        productCreatedOutput.send(
                withPayload(new ProductCreatedEvent(toDto(product))).build()
        );
        return product;
    }

    public Product publishProductUpdatedEvent(Product oldProduct, Product updatedProduct) {

        productUpdatedOutput.send(
                withPayload(new ProductUpdatedEvent(toDto(oldProduct), toDto(updatedProduct))).build())
        ;
        return updatedProduct;
    }

    public Product publishProductReviewAddedEvent(Product product, Review review) {

        productReviewAddedOutput.send(
                withPayload(new ProductReviewAddedEvent(toDto(product), toDto(review))).build()
        );
        return product;
    }

    private ProductDto toDto(Product product) {
        return ProductDto.builder()
                .id(product.getId())
                .name(product.getName())
                .brand(product.getBrand())
                .category(product.getCategory().name())
                .description(product.getDescription())
                .price(product.getPrice())
                .createdAt(product.getCreatedAt())
                .lastModifiedAt(product.getLastModifiedAt())
                .build();
    }

    private ReviewDto toDto(Review review) {
        return ReviewDto.builder()
                .rating(review.getRating())
                .comment(review.getComment())
                .createdBy(review.getCreatedBy())
                .createdAt(review.getCreatedAt())
                .build();
    }
}
