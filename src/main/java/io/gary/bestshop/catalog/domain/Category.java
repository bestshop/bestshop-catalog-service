package io.gary.bestshop.catalog.domain;

public enum Category {

    BooksAudible,
    MusicMovies,
    Electronics,
    Computers,
    Office,
    HomeGarden,
    FoodGrocery,
    BeautyHealth,
    BabyToyKids,
    ClothingShoes,
    SportsOutdoors

}
